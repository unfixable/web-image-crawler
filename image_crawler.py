from datetime import date
from tkinter import *
import os
from PIL import ImageTk, Image
from icrawler.builtin import BaiduImageCrawler, BingImageCrawler, GoogleImageCrawler, FlickrImageCrawler


class User_Interface:
    def __init__(self, master):
        self.master = master
        master.title("Image Crawler")

        frame = Frame(master)

        self.total = 0
        self.entered_keyword = ""
        self.entered_number = 0
        self.files = []
        self.file_idx = 0
        self.picture_idx = 0
        self.checking_pictures = False

        # image border
        self.bar = Frame(root, relief=RIDGE, borderwidth=5)

        self.progress_label_text = StringVar()
        self.progress_label_text.set(self.total)
        self.progress_label_text = Label(
            master, textvariable=self.progress_label_text)

        self.label_keyword = Label(master, text="Keyword:")
        self.label_no_image = Label(
            master, text="Number of Images (Maximum 1000):")
        self.label_progress = Label(master, text="Progress:")

        vKeyword = master.register(self.validate_keyword)
        vNumber = master.register(self.validate_number)

        self.keyword_entry = Entry(
            master, validate="key", validatecommand=(vKeyword, '%P'))
        self.number_entry = Entry(
            master, validate="key", validatecommand=(vNumber, '%P'))

        self.crawl_google_button = Button(
            master, text="Crawl Google Images", command=lambda: self.crawl_google())
        self.crawl_bing_button = Button(
            master, text="Crawl Bing Images", command=lambda: self.crawl_bing())
        self.crawl_baidu_button = Button(
            master, text="Crawl Baidu Images", command=lambda: self.crawl_baidu())
        self.crawl_flickr_button = Button(
            master, text="Crawl Flickr Images", command=lambda: self.crawl_flickr())

        self.accept_image_button = Button(self.master, text=" YES ", command=lambda: self.accept_image(),
                                          state=DISABLED, height=8, width=25, fg='BLACK', bg='gray85')
        self.reject_image_button = Button(self.master, text=" NO ", command=lambda: self.reject_image(), state=DISABLED,
                                          height=8, width=25, fg='BLACK', bg='gray85')

        self.label_flickr_note = Label(master,
                                       text="Note: Flickr search is very slow: 1-10sec/picture. Crawling lot's of images will take a long time")

        self.label_status = Label(
            master, text="Select keyword, number of pictures and search engine", fg='red')

        # white default image to initialize the image label
        self.imgPath = "000001.jpg"
        image = Image.open(self.imgPath)
        image = image.resize((400, 400), Image.ANTIALIAS)

        self.icon = ImageTk.PhotoImage(image)
        self.icon_size = Label(self.bar, image=self.icon,
                               width=400, height=400)

        # LAYOUT

        self.label_keyword.grid(row=0, column=0)
        self.keyword_entry.grid(row=0, column=1)

        self.label_no_image.grid(row=1, column=0)
        self.number_entry.grid(row=1, column=1)

        self.crawl_google_button.grid(row=3, column=0)
        self.crawl_bing_button.grid(row=4, column=0)
        self.crawl_baidu_button.grid(row=5, column=0)
        self.crawl_flickr_button.grid(row=6, column=0)
        self.label_status.grid(row=7, column=0)

        self.bar.grid(row=8, column=0)
        self.icon_size.grid(row=8, column=0)

        self.accept_image_button.grid(row=8, column=1)
        self.reject_image_button.grid(row=8, column=2)
        self.label_flickr_note.grid(row=9, column=0)

    def validate_number(self, new_text):
        if not new_text:
            self.entered_number = 0
            return True

        try:
            self.entered_number = int(new_text)
            return True
        except ValueError:
            return False

    def validate_keyword(self, new_text):

        try:
            self.entered_keyword = str(new_text)
            return True
        except ValueError:
            return False

    def crawl_google(self):
        self.create_folders()
        self.label_status.config(text='Crawling - Please wait')
        google_crawler = GoogleImageCrawler(parser_threads=2, downloader_threads=4,
                                            storage={'root_dir': 'candidates/' + self.entered_keyword})
        google_crawler.crawl(keyword=self.entered_keyword, max_num=self.entered_number,
                             min_size=(250, 250), max_size=(1280, 1280))
        self.load_pictures_in_list()

    def crawl_bing(self):
        self.create_folders()
        self.label_status.config(text='Crawling - Please wait')
        bing_crawler = BingImageCrawler(downloader_threads=4,
                                        storage={'root_dir': 'candidates/' + self.entered_keyword})
        bing_crawler.crawl(keyword=self.entered_keyword, offset=0, max_num=self.entered_number,
                           min_size=(500, 500), max_size=None)
        self.load_pictures_in_list()

    def crawl_baidu(self):
        self.create_folders()
        self.label_status.config(text='Crawling - Please wait')
        baidu_crawler = BaiduImageCrawler(
            storage={'root_dir': 'candidates/' + self.entered_keyword})
        baidu_crawler.crawl(keyword=self.entered_keyword, offset=0, max_num=self.entered_number,
                            min_size=(500, 500), max_size=None)
        self.load_pictures_in_list()

    def crawl_flickr(self):
        self.create_folders()
        self.label_status.config(text='Crawling - Please wait')
        flickr_crawler = FlickrImageCrawler('ee09c459f9467cfcfa3c721e406e5cde',
                                            storage={'root_dir': 'candidates/' + self.entered_keyword})
        flickr_crawler.crawl(max_num=self.entered_number, tags=self.entered_keyword,
                             min_upload_date=date(2014, 1, 1))
        self.load_pictures_in_list()

    def create_folders(self):
        directory = 'candidates/' + self.entered_keyword
        if not os.path.exists(directory):
            os.makedirs(directory)

        directory = 'images/' + self.entered_keyword
        if not os.path.exists(directory):
            os.makedirs(directory)

    def load_pictures_in_list(self):
        self.files = [file_i
                      for file_i in os.listdir('candidates/' + self.entered_keyword)]
        self.file_idx = 0
        self.picture_idx = 0
        self.setup_picture_review()
        self.checking_pictures = True
        self.check_pictures()

    def check_pictures(self):
        print("Picture: " + str(self.file_idx))
        print("Total: " + str(len(self.files)))

        self.label_status.config(
            text='Please select if labels are correct or not (YES/NO) (' + str(self.file_idx) + '/' + str(
                len(self.files)) + ')')

        if (self.file_idx < len(self.files)):
            file_path = 'candidates/' + self.entered_keyword + \
                '/' + self.files[self.file_idx]
            new_image = Image.open(file_path)
            new_image = new_image.resize((400, 400), Image.ANTIALIAS)

            # height, width, channels = scipy.ndimage.imread(file_path).shape
            # if(channels > 3):
            #    print("RBGA detected")
            #    self.reject_image()

            new_image = ImageTk.PhotoImage(new_image)

            self.icon_size.configure(image=new_image)
            self.icon_size.image = new_image
        else:
            self.accept_image_button.configure(
                state=DISABLED, fg='BLACK', bg='gray85')
            self.reject_image_button.configure(
                state=DISABLED, fg='BLACK', bg='gray85')
            self.crawl_google_button.configure(state=NORMAL)
            self.crawl_flickr_button.configure(state=NORMAL)
            self.crawl_baidu_button.configure(state=NORMAL)
            self.crawl_bing_button.configure(state=NORMAL)
            new_image = Image.open('000001.jpg')
            new_image = new_image.resize((400, 400), Image.ANTIALIAS)
            new_image = ImageTk.PhotoImage(new_image)
            self.icon_size.configure(image=new_image)
            self.icon_size.image = new_image
            self.checking_pictures = False
            self.label_status.config(
                text='Select keyword, number of pictures and search engine')

    def setup_picture_review(self):
        self.check_pictures()
        self.accept_image_button.configure(
            state=NORMAL, fg='WHITE', bg='GREEN')
        self.reject_image_button.configure(state=NORMAL, fg='WHITE', bg='RED')
        self.crawl_google_button.configure(state=DISABLED)
        self.crawl_flickr_button.configure(state=DISABLED)
        self.crawl_baidu_button.configure(state=DISABLED)
        self.crawl_bing_button.configure(state=DISABLED)

    def accept_image(self):
        new_image = Image.open(
            'candidates/' + self.entered_keyword + '/' + self.files[self.file_idx])
        save_path = 'images/' + self.entered_keyword + '/'
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        # We check for existing files first to not overwrite in case of multiple searches with same keyword
        while (os.path.isfile(save_path + str(self.picture_idx).zfill(6) + '.jpg')):
            self.picture_idx += 1

        new_image = new_image.convert('RGB')
        new_image.save(save_path + str(self.picture_idx).zfill(6) + '.jpg')
        self.delete_current_image()
        self.file_idx += 1
        self.picture_idx += 1
        self.check_pictures()

    def reject_image(self):
        self.delete_current_image()
        self.file_idx += 1
        self.check_pictures()

    def delete_current_image(self):
        file_path = 'candidates/' + self.entered_keyword + \
            '/' + self.files[self.file_idx]
        try:
            os.remove(file_path)
        except OSError:
            pass

    # def reject_callback(self,e):
    #     print("hello")
    #     self.reject_image()

    # def accept_callback(self,e):
    #     print("hello")
    #     self.accept_image()


def getopts(argv):
    opts = {}
    while argv:
        if argv[0][0] == '-':
            opts[argv[0]] = argv[1]
        argv = argv[1:]
    return opts


if __name__ == '__main__':
    from sys import argv

    myargs = getopts(argv)
    if '-i' in myargs:
        print(myargs['-i'])


img_keyword = ""
img_number = 0

for key in myargs:
    if key == "-name":
        img_keyword = myargs[key]
    elif key == "-number":
        img_number = int(myargs[key])

root = Tk()
root.geometry('{}x{}'.format(910, 610))

my_gui = User_Interface(root)
root.mainloop()

