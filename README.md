# Web Image Crawler

Python GUI application that crawls images by keywords from various search engines. Initial developed for obtaining food images for a personal project but can be used for any keyword.
Built using tkinter and icrawler/beautifulsoup

![](screenshots/Capture1.JPG)

Supports google/bing/baidu/flickr searches.

**Please note** :Currently google search is not functional. If needed, further information on how to implement can be found here: https://github.com/hellock/icrawler/issues/65


After crawling the user can manually check the images. Accepted images can be found under **./images/search_keyword/**

Before checking, the images they can be found under **./candidates/search_keyword**


**Binary download here:** https://drive.google.com/file/d/1ubMWCHQNGyzK9nwTupoT-csbTw3d-iTQ/view?usp=sharing